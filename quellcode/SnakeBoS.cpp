// BoS1.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>


int const g = 20;

//coords
int headsX = 5;
int xDirection = 1;
int headsY = 5;
int yDirection = 0;

//break point variables
int gameOver = 0;

//other variables
int invertSpeed;
int snakeLength = 3;
char move = 48;
char lastMove = 'd';
int rndx, rndy;
int points = 0;
int i2 = 0;
int dotOnSnake = 0;


struct Snake
{
	int x[g*g];
	int y[g*g];
};
Snake snake1;

//generates random dot while avoiding the spawn of it on the snake
void rndDot(int *rndx, int *rndy) {
	time_t t;
	/* Intializes random number generator */
	srand((unsigned)time(&t));

	while (true)
	{
		for (size_t i = 0; i < snakeLength; i++)
		{
			if (*rndx == snake1.x[i] && *rndy == snake1.y[i]) {
				*rndx = rand() % g;
				*rndy = rand() % g;
				dotOnSnake = 1;
				break;
			}
			else
				dotOnSnake = 0;
		}
		if (dotOnSnake == 0)
			break;
	}

	farbe2(*rndx, *rndy, RED);
	form2(*rndx, *rndy, "c");
}

void startNewGame() {
	loeschen();
	groesse(g, g);
	formen("s");
	headsX = 5;
	xDirection = 1;
	headsY = 5;
	yDirection = 0;
	gameOver = 0;
	snakeLength = 3;
	invertSpeed = 100;
	lastMove = 'd';
	points = 0;
	i2 = 0;
	flaeche(SILVER);
	rahmen(BLACK);
	rndDot(&rndx, &rndy);

	for (size_t i = 0; i < g; i++)
	{
		snake1.x[i] = 0;
		snake1.y[i] = 0;
	}
	snake1.x[0] = headsX;
	snake1.y[0] = headsY;
}

int _tmain(int argc, _TCHAR* argv[])
{

	printf("\nPress any key to start...\nIngame press space to pause\n");
	_getch();
	startNewGame();

	while (true)
	{
		//snakes tail gets overcolored
		if (i2 == snakeLength)
			farbe2(snake1.x[0], snake1.y[0], SILVER);

		//draws snake
		farbe2(headsX, headsY, GREEN);
		form2(headsX, headsY, "s");

		//animation
		Sleep(invertSpeed);
		

		//the snakes moves and pause
		if (_kbhit())
		{
			move = _getch();
			switch (move)
			{
			case 'w':
				if (lastMove != 'w' && lastMove != 's') yDirection = 1;
				xDirection = 0;
				lastMove = 'w';
				break;
			case 'a':
				if (lastMove != 'd' && lastMove != 'a') xDirection = (-1);
				yDirection = 0;
				lastMove = 'a';
				break;
			case 's':
				if (lastMove != 'w' && lastMove != 's') yDirection = (-1);
				xDirection = 0;
				lastMove = 's';
				break;
			case 'd':
				if (lastMove != 'a' && lastMove != 'd') xDirection = 1;
				yDirection = 0;
				lastMove = 'd';
				break;
			case 32:
				printf("\nPaused\n");
				while (_getch() != 32);
			}
		}
		headsY += yDirection;
		headsX += xDirection;
		
		//saves x and y coords to struct snake1...
		i2++;
		snake1.x[i2] = headsX;
		snake1.y[i2] = headsY;
		//...until arrays are as big as the snakes length
		if (i2 == snakeLength + 1) {
			for (size_t i = 0; i < snakeLength + 1; i++)
			{
				snake1.x[i] = snake1.x[i + 1];
				snake1.y[i] = snake1.y[i + 1];
			}
			i2 = snakeLength;
		}

		//game over if...
		//...snake bites itself...
		if (i2 == snakeLength && snakeLength > 4) {
			for (size_t k = 0; k < snakeLength; k++)
			{
				if (headsX == snake1.x[k] && headsY == snake1.y[k])
					gameOver = 1;
			}
		}
		//...out of area.
		if (headsX < 0 || headsX > g - 1 || headsY < 0 || headsY > g - 1)
			gameOver = 1;

		//snake eats dot + increase difficult + spawn new dot + show points
		if (rndx == headsX && rndy == headsY) {
			snakeLength++;
			if (invertSpeed > 10)
				invertSpeed -= 5;
			rndDot(&rndx, &rndy);
			points += 100;
			printf("\rPoints: %i", points);
		}
		

		//New Game?
		if (gameOver == 1) {
			printf("\nGame Over... \nNew Game ? y / n \n");
			headsX -= xDirection;
			headsY -= yDirection;
			farbe2(headsX, headsY, RED);
			while (move != 'y' || move != 'n') {
				move = _getch();
				if (move == 'y') {
					startNewGame();
					break;
				}
				if (move == 'n') {
					gameOver = 1;
					break;
				}
			}
		}
		
		if (gameOver == 1) break;
	}
	return 0;
}