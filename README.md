||Wilkommen beim Projekt SnakeBoS.||
====================================

1. SnakeBoS ist eine Anlehnung an den Spieleklassiker Snake*.
2. In der Sprache C programmiert, wird SnakeBoS mit Hilfe
der zusätzlichen Software Board of Symbols grafisch dargestellt.

3. SnakeBoS erlaubt es dem Spieler die Schlange mit den Tasten
'W,A,S,D' zu steuern.
Ein zufälliger "Apfel" erscheint auf dem Spielfeld.
Sobald dieser von der Schlange "gegessen" wird,
vergrößert sie sich und wird leicht schneller.
Sobald sich die Schlange in den eigenen Schwanz "beißt"
oder in eine der vier Wände bewegt wird,
ist das Spiel vorbei.

4. Nach meinem Wissensstand in der Programmierung mit C
wird die größte Herausforderung bei diesem Projekt
die Darstellung der sich vergrößernden Schlange.

Ein erster Lösungsansatz dieser Schwierigkeit wäre,
die einzelnen Körperteile der Schlange in einem Array zu speichern
und diese dann bei jedem Schritt der Schlange neu auszugeben oder
bei jedem Schritt den letzten Teil der Schlange zu "überfärben".

*Snake ähnlich den 90er Nokia Handy-Modellen.