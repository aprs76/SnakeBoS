// BoS1.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <string.h>

int const g = 25;

//coords
int headX = 5;
int xDirection = 1;
int headY = 5;
int yDirection = 0;

//break point variables
int gameOver = 0;

//other variables
int framePause;
int snakeLength = 3;
char move = 48;
char lastMove = 'd';
int rndx, rndy;
int points = 0;
int i2 = 0;
int dotOnSnake = 0;
char inputName[20] = "";
FILE *highscores;

struct user {
	int score;
	char name[20] = "";
};
user top[11];

struct Snake {
	int x[g*g];
	int y[g*g];
};
Snake snake1;

//generates random dot while avoiding the spawn of it on the snake
void rndDot(int *rndx, int *rndy) {
	time_t t;
	/* Intializes random number generator */
	srand((unsigned)time(&t));

	while (true)
	{
		for (size_t i = 0; i < snakeLength; i++)
		{
			if (*rndx == snake1.x[i] && *rndy == snake1.y[i]) {
				*rndx = rand() % g;
				*rndy = rand() % g;
				dotOnSnake = 1;
				break;
			}
			else
				dotOnSnake = 0;
		}
		if (dotOnSnake == 0)
			break;
	}

	farbe2(*rndx, *rndy, RED);
	form2(*rndx, *rndy, "c");
}

//own method for fopen to reduce code
void fopenHighscores(const char*_Mode) {
	fopen_s(&highscores, "C:\\Users\\Andrej\\Documents\\Visual Studio 2015\\Projects\\SnakeBoS\\highscores.txt",_Mode);
	if (highscores == NULL)
		printf("\nError, could not open highscores.txt!\n");
}

//shows top 10
void showHighscores() {
	char output[200] = "";
	int i = 0;
	fopenHighscores("r");
	rewind(highscores);

	printf("============================================\n");
	while (fgets(output, 200, highscores) != NULL)
	{
		i++;
		printf("%i. %s", i, output);
	}
	printf("============================================\n");
	_fcloseall();
}

//loading highscores into struct user top10
void loadHighscores() {
	char output[200] = "";
	int i = 0;
	for (i = 0; i < 11; i++)
	{
		top[i].score = 0;
		for (size_t c = 0; c < 20; c++)
		{
			top[i].name[c] = ' ';

		}
	}
	i = 0;
	fopenHighscores("r");
	rewind(highscores);
	while (fgets(output, 200, highscores) != NULL) {
		sscanf_s(output, "%d", &top[i].score);
		for (size_t c = 0; c < strlen(output); c++) {
			if (output[c] > 64)
				top[i].name[c] = output[c];
		}
		i++;
	}
	_fcloseall();
}

//write new highscores List
void writeNewHighscoreList() {
	int i = 0;
	fopenHighscores("w");
	for (i = 0; i < 10; i++)
	{
		fprintf(highscores, "%d ", top[i].score);
		for (size_t c = 0; c < 20; c++)
		{
			if (top[i].name[c] > 64)
				fprintf(highscores, "%c", top[i].name[c]);
		}
		fprintf(highscores, "\n");
	}

	_fcloseall();
}


void startNewGame() {
	loeschen();
	groesse(g, g);
	formen("s");
	headX = 5;
	xDirection = 1;
	headY = 5;
	yDirection = 0;
	gameOver = 0;
	snakeLength = 3;
	framePause = 50;
	lastMove = 'd';
	points = 0;
	i2 = 0;
	flaeche(SILVER);
	rahmen(BLACK);
	rndDot(&rndx, &rndy);

	for (size_t i = 0; i < g; i++)
	{
		snake1.x[i] = 0;
		snake1.y[i] = 0;
	}
	snake1.x[0] = headX;
	snake1.y[0] = headY;
	
}

int _tmain(int argc, _TCHAR* argv[])
{
	printf("\nWelcome to SnakeBoS\n"
		"s = start\n"
		"h = highscores\n"
		"f = start with features\n"
		"space = pause\n");
	while (move != 's' || move != 'h') {
		move = _getch();
		if (move == 's') {
			startNewGame();
			break;
		}
		if (move == 'f') {
			startNewGame();
			break;
		}
		if (move == 'h') {
			showHighscores();
		}
	}

	while (true)
	{
		//snakes tail gets overcolored
		if (i2 == snakeLength)
			farbe2(snake1.x[0], snake1.y[0], SILVER);

		//draws snake
		farbe2(headX, headY, GREEN);
		form2(headX, headY, "s");

		//animation
		Sleep(framePause);
		

		//the snakes moves and pause
		if (_kbhit())
		{
			move = _getch();
			switch (move)
			{
			case 'w':
				if (lastMove != 'w' && lastMove != 's') yDirection = 1;
				xDirection = 0;
				lastMove = 'w';
				break;
			case 'a':
				if (lastMove != 'd' && lastMove != 'a') xDirection = (-1);
				yDirection = 0;
				lastMove = 'a';
				break;
			case 's':
				if (lastMove != 'w' && lastMove != 's') yDirection = (-1);
				xDirection = 0;
				lastMove = 's';
				break;
			case 'd':
				if (lastMove != 'a' && lastMove != 'd') xDirection = 1;
				yDirection = 0;
				lastMove = 'd';
				break;
			case 32:
				while (_getch() != 32);
			}
		}
		headY += yDirection;
		headX += xDirection;
		
		//saves x and y coords to struct snake1...
		i2++;
		snake1.x[i2] = headX;
		snake1.y[i2] = headY;
		//...until arrays are as big as the snakes length
		if (i2 == snakeLength + 1) {
			for (size_t i = 0; i < snakeLength + 1; i++)
			{
				snake1.x[i] = snake1.x[i + 1];
				snake1.y[i] = snake1.y[i + 1];
			}
			i2 = snakeLength;
		}

		//game over if...
		//...snake bites itself...
		if (i2 == snakeLength && snakeLength > 4) {
			for (size_t k = 0; k < snakeLength; k++)
			{
				if (headX == snake1.x[k] && headY == snake1.y[k])
					gameOver = 1;
			}
		}
		//...out of area.
		if (headX < 0 || headX > g - 1 || headY < 0 || headY > g - 1)
			gameOver = 1;

		//snake eats dot + increase difficult + spawn new dot + show points
		if (rndx == headX && rndy == headY) {
			snakeLength++;
			//if (invertSpeed > 10)
			//	invertSpeed -= 5;
			rndDot(&rndx, &rndy);
			points += 100;
			printf("\rPoints: %i", points);
		}
		

		//New Game?
		if (gameOver == 1) {
			headX -= xDirection;
			headY -= yDirection;
			farbe2(headX, headY, RED);

			//add new score in correct Place
			printf("\nGame Over... \n");
			int i = 0;
			if (points >= top[9].score) {
				loadHighscores();
				printf("You are in the Top 10! Enter your name:\n");
				fgets(inputName, 20, stdin);
				for (i = 9; i >= 0; i--)
				{
					if (points <= top[i].score) break;
					top[i + 1] = top[i];
					top[i].score = points;
					strcpy_s(top[i].name, inputName);
				}
				writeNewHighscoreList();
				showHighscores();
			}
			printf("Again? y / n\n");
			while (move != 'y' || move != 'n') {
				move = _getch();
				if (move == 'y') {
					startNewGame();
					break;
				}
				if (move == 'n') {
					gameOver = 1;
					break;
				}
			}
		}
		
		if (gameOver == 1) break;
	}
	return 0;
}