Das Team besteht aus folgenden Personen: Andrej Preis (Ich).

Ich bin für folgende Aufgaben zuständig:
- ermitteln von Lösungen für möglich auftauchende Probleme
- Bugfixes
- Code-Optimierung (Variablennamen, Erstellung von Funktionen)
- Kommentare im Code
- Grafikdesign
- zusätliche Spiel-Features